# SRT gateway

SRT (Secure Reliable Transport) gateway for restream publisher stream to multiple clients without demuxing.

**_In early state of development, it's not recommended to use in production_**

## Build

Minimal supported version `libsrt` is 1.4.2, then it's recommended to set cmake parameter `SRT_USE_SYSTEM` to `OFF` - build static linked library of libsrt from submodule source code.

```
git submodule init --recursive
mkdir build && cd build
cmake -DSRT_USE_SYSTEM=OFF ..
make
```

## Configuration file
TODO

## Usage
```
./srt-server
```