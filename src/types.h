#ifndef __SRT_SERVER_TYPES_H__
#define __SRT_SERVER_TYPES_H__

#include <libconfig.h>
#include <srt.h>
#include <stdbool.h>
#include <utarray.h>
#include <uthash.h>

#define MAX_PASSPHRASE_LENGTH 79
#define MAX_STREAMID_LENGTH 512

typedef enum ip_version
{
	IP_VERSION_IPV4_ONLY,
	IP_VERSION_IPV6_ONLY,
	IP_VERSION_BOTH,
	ENUM_IP_VERSION_COUNT
} IpVersion;

typedef enum full_buffer_behaviour
{
	FULL_BUFFER_WAIT,
	FULL_BUFFER_DROP,
	FULL_BUFFER_KILL,
	ENUM_FULL_BUFFER_COUNT
} FullBufferBehaviour;

typedef enum exit_code
{
	/*
	From stdlib.h:
	EXIT_SUCCESS,
	EXIT_FAILURE
	*/
	EXIT_INVALID_ALLOC = EXIT_FAILURE + 1,
	EXIT_SRT_ERROR,
	EXIT_PTHREAD_ERROR
} ExitCode;

typedef struct conf
{
	// TODO: Latency
	// TODO: Allow/Deny list
	// TODO: Statistics

	config_t config;

	int log_level;

	unsigned short publish_port;
	unsigned short client_port;

	IpVersion ip;

	char publish_passphrase[MAX_PASSPHRASE_LENGTH];
	char client_passphrase[MAX_PASSPHRASE_LENGTH];

	char default_app_name[MAX_STREAMID_LENGTH];

	bool accept_unknown_app;

	bool keep_alive;
	FullBufferBehaviour full_buffer;
} Conf;

typedef struct client
{
	SRTSOCKET socket;

	// for logging purpose
	char ip[INET6_ADDRSTRLEN];
	unsigned short port;

	struct client* next;
	struct client* prev;
} Client;

typedef struct app
{
	/* CONFIG */
	char name[MAX_STREAMID_LENGTH];

	char publish_passphrase[MAX_PASSPHRASE_LENGTH];
	char client_passphrase[MAX_PASSPHRASE_LENGTH];

	bool keep_alive;
	FullBufferBehaviour full_buffer;

	/* SERVER DATA */
	Client* publish_socket; // connection with publishing client
	pthread_t thread;
	int buffer_size;
	char* buffer;

	Client* clients;

	pthread_mutex_t waiting_clients_mutex;
	Client* waiting_clients; // client waiting to the end of send loop (thread-safe)

	UT_hash_handle hh;
} App;

typedef struct server
{
	Conf* config;

	SRTSOCKET publish_server;
	SRTSOCKET client_server;

	pthread_t publish_listen_thread;
	pthread_t client_listen_thread;

	App* apps; // hash table
} Server;

#endif
