#ifndef __SRT_SERVER_UTILS_H__
#define __SRT_SERVER_UTILS_H__

#include "types.h"
#include <srt.h>
#include <stdio.h>

#define print_error(message, func, file, line)                                                                         \
	fprintf(stderr, "Error: %s\n\t in %s (%s:%d)\n", message, func, file, line)

#define print_warning(message, func, file, line)                                                                       \
	fprintf(stderr, "Warning: %s\n\t in %s (%s:%d)\n", message, func, file, line)

#define error_assert(condition, message, exit_value)                                                                   \
	if (condition)                                                                                                     \
	{                                                                                                                  \
		print_error(message, __func__, __FILE__, __LINE__);                                                            \
		exit(exit_value);                                                                                              \
	}

#define error_assert_srt(status)                                                                                       \
	if ((status) == SRT_ERROR)                                                                                         \
	{                                                                                                                  \
		print_error(srt_getlasterror_str(), __func__, __FILE__, __LINE__);                                             \
		exit(EXIT_SRT_ERROR);                                                                                          \
	}

#define warning_assert_srt(status, event)                                                                              \
	if ((status) == SRT_ERROR)                                                                                         \
	{                                                                                                                  \
		print_warning(srt_getlasterror_str(), __func__, __FILE__, __LINE__);                                           \
		event;                                                                                                         \
	}

void* malloc_safe(size_t size);

#endif
