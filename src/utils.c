#include "utils.h"
#include "types.h"
#include <stdlib.h>

void* malloc_safe(size_t size)
{
	void* memory = malloc(size);
	error_assert(memory == NULL, "Invalid allocation", EXIT_INVALID_ALLOC);
	return memory;
}