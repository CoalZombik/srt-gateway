#include "conf.h"
#include "server.h"
#include "types.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

static Conf* conf = NULL;
static Server* server = NULL;

void sigint_event(int sig)
{
	if (server != NULL)
		server_stop(server);

	if (conf != NULL)
		conf_unload(&conf);

	exit(0);
}

void set_sigint_event()
{
	struct sigaction action;
	action.sa_handler = sigint_event;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;

	sigaction(SIGINT, &action, NULL);
}

int main(int argc, char** argv)
{
	set_sigint_event();

	conf = conf_load("test.conf"); // TODO: load file from specified location
	server = server_init(conf);

	while (true)
		pause();
	return 0;
}