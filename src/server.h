#ifndef __SRT_SERVER_SERVER_H__
#define __SRT_SERVER_SERVER_H__

#include "types.h"

Server* server_init(Conf* conf);

void server_stop(Server* server);

#endif