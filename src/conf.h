#ifndef __SRT_SERVER_CONF_H__
#define __SRT_SERVER_CONF_H__

#include "types.h"
#include <stdbool.h>

Conf* conf_load(const char* path);

bool conf_fill_app(Conf* conf, App* app);

void conf_unload(Conf** conf);

#endif