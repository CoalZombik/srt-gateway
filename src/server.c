#include "conf.h"
#include "types.h"
#include "utils.h"
#include <access_control.h>
#include <inttypes.h>
#include <pthread.h>
#include <srt.h>
#include <stdlib.h>
#include <string.h>
#include <uthash.h>
#include <utlist.h>

// TODO: App garbage collector

void create_address_ipv4(struct sockaddr_in* addr, unsigned short port)
{
	addr->sin_addr.s_addr = INADDR_ANY;
	addr->sin_family = AF_INET;
	addr->sin_port = htons(port);
}

void create_address_ipv6(struct sockaddr_in6* addr, unsigned short port)
{
	addr->sin6_addr = in6addr_any;
	addr->sin6_family = AF_INET6;
	addr->sin6_port = htons(port);
	addr->sin6_flowinfo = 0;
	addr->sin6_scope_id = 0;
}

struct sockaddr_storage create_address(unsigned short port, bool ipv6, int* addr_len)
{
	struct sockaddr_storage output;

	if (ipv6)
	{
		create_address_ipv6((struct sockaddr_in6*)&output, port);
		*addr_len = sizeof(struct sockaddr_in6);
	}
	else
	{
		create_address_ipv4((struct sockaddr_in*)&output, port);
		*addr_len = sizeof(struct sockaddr_in);
	}

	return output;
}

Client* client_init(SRTSOCKET socket, struct sockaddr_storage* addr)
{
	Client* client = malloc_safe(sizeof(Client));
	client->socket = socket;

	if (addr->ss_family == AF_INET)
	{
		struct sockaddr_in* addr_ipv4 = (struct sockaddr_in*)addr;
		inet_ntop(AF_INET, &addr_ipv4->sin_addr, client->ip, INET6_ADDRSTRLEN);
		client->port = addr_ipv4->sin_port;
	}
	else if (addr->ss_family == AF_INET6)
	{
		struct sockaddr_in6* addr_ipv6 = (struct sockaddr_in6*)addr;
		inet_ntop(AF_INET6, &addr_ipv6->sin6_addr, client->ip, INET6_ADDRSTRLEN);
		client->port = addr_ipv6->sin6_port;
	}
	else
	{
		strcpy(client->ip, "unknown_ip");
		client->port = 0;
	}

	client->next = NULL;
	client->prev = NULL;
}

void client_free(Client* client)
{
	srt_close(client->socket);
	free(client);
}

void client_list_free(Client** list)
{
	Client* tmp;
	Client* item;
	DL_FOREACH_SAFE(*list, item, tmp) { client_free(item); }
}

App* app_init(const char* streamid)
{
	App* app = (App*)malloc_safe(sizeof(App));

	strcpy(app->name, streamid);

	app->publish_socket = NULL;
	app->thread = 0;
	app->buffer_size = 0;
	app->buffer = 0;

	app->clients = NULL;
	pthread_mutex_init(&app->waiting_clients_mutex, NULL);
	app->waiting_clients = NULL;

	return app;
}

void app_free(App* app)
{
	if (app->thread)
	{
		if (app->publish_socket != NULL)
			srt_close(app->publish_socket->socket); // close publisher socket to stop thread
		pthread_join(app->thread, NULL);
	}

	if (app->publish_socket != NULL)
		client_free(app->publish_socket);
	if (app->buffer != NULL)
		free(app->buffer);

	client_list_free(&app->clients);
	client_list_free(&app->waiting_clients);

	pthread_mutex_destroy(&app->waiting_clients_mutex);

	free(app);
}

SRTSOCKET create_socket(unsigned short port, IpVersion ip, srt_listen_callback_fn* accept_event, void* event_arg)
{
	SRTSOCKET socket;
	error_assert_srt(socket = srt_create_socket());

	int addr_len;
	struct sockaddr_storage addr = create_address(port, ip != IP_VERSION_IPV4_ONLY, &addr_len);

	int32_t ipv6_only = ip == IP_VERSION_IPV6_ONLY ? 1 : 0;
	error_assert_srt(srt_setsockflag(socket, SRTO_IPV6ONLY, &ipv6_only, sizeof(ipv6_only)));

	error_assert_srt(srt_bind(socket, (struct sockaddr*)&addr, addr_len));
	error_assert_srt(srt_listen(socket, 5));

	error_assert_srt(srt_listen_callback(socket, accept_event, event_arg));

	return socket;
}

void start_accept_thread(pthread_t* thread, void* (*accept_thread)(void* arg), void* accept_arg)
{
	error_assert(pthread_create(thread, NULL, accept_thread, accept_arg) != 0, "pthread_create failed",
				 EXIT_PTHREAD_ERROR);
}

App* validate_peer(void* opaque, SRTSOCKET ns, int hs_version, const struct sockaddr* peeraddr, const char* streamid,
				   bool publisher)
{
	Server* server = (Server*)opaque;

	char app_name[MAX_STREAMID_LENGTH];
	if (strlen(streamid) == 0) // default app
		strcpy(app_name, server->config->default_app_name);
	else
		strcpy(app_name, streamid);

	App* app = NULL;
	HASH_FIND_STR(server->apps, app_name, app);

	if (app == NULL)
	{
		app = app_init(app_name);
		fprintf(stderr, "validate_peer: init new app %s\n", app_name);

		bool in_config = conf_fill_app(server->config, app);

		if (!in_config && !server->config->accept_unknown_app) // reject unknown app
		{
			fprintf(stderr, "unknown app\n");
			app_free(app);

			srt_setrejectreason(ns, SRT_REJX_UNACCEPTABLE);
			return NULL;
		}

		HASH_ADD_STR(server->apps, name, app);
	}

	char* passphrase = publisher ? app->publish_passphrase : app->client_passphrase;
	fprintf(stderr, "validate_peer: passphrase=%s\n", passphrase);
	error_assert_srt(srt_setsockflag(ns, SRTO_PASSPHRASE, passphrase, strlen(passphrase)));

	/*SRT_TRANSTYPE transtype = SRTT_LIVE;
	error_assert_srt(srt_setsockflag(ns, SRTO_TRANSTYPE, &transtype, sizeof(transtype)));*/

	return app;
}

int validate_peer_publish(void* opaque, SRTSOCKET ns, int hs_version, const struct sockaddr* peeraddr,
						  const char* streamid)
{
	App* app = validate_peer(opaque, ns, hs_version, peeraddr, streamid, true);

	if (app == NULL)
		return SRT_EUNKNOWN;
	if (app->publish_socket != NULL)
	{
		fprintf(stderr, "app already have a publisher\n");
		srt_setrejectreason(ns, SRT_REJX_CONFLICT);
		return SRT_EUNKNOWN;
	}
	return SRT_SUCCESS;
}

int validate_peer_client(void* opaque, SRTSOCKET ns, int hs_version, const struct sockaddr* peeraddr,
						 const char* streamid)
{
	App* app = validate_peer(opaque, ns, hs_version, peeraddr, streamid, false);
	if (app == NULL)
		return SRT_EUNKNOWN;
	if ((!app->keep_alive && app->publish_socket == NULL)) // check keep_alive
	{
		fprintf(stderr, "app haven't a publisher\n");
		srt_setrejectreason(ns, SRT_REJX_NOTFOUND);
		return SRT_EUNKNOWN;
	}
	return SRT_SUCCESS;
}

void* publish_send_thread(void* arg)
{
	App* app = (App*)arg;

	SRT_MSGCTRL metadata;
	int recv_bytes = 0;

	fprintf(stderr, "publish_send_thread: start %s\n", app->name);
	while ((recv_bytes = srt_recvmsg2(app->publish_socket->socket, app->buffer, app->buffer_size, &metadata)) > 0)
	{
		// copy clients from waiting array
		pthread_mutex_lock(&app->waiting_clients_mutex);
		DL_CONCAT(app->clients, app->waiting_clients);
		app->waiting_clients = NULL;
		pthread_mutex_unlock(&app->waiting_clients_mutex);

		Client* client;
		Client* tmp;
		DL_FOREACH_SAFE(app->clients, client, tmp)
		{
			SRT_MSGCTRL metadata_client = srt_msgctrl_default;
			metadata_client.srctime = metadata.srctime;

			int send_bytes = srt_sendmsg2(client->socket, app->buffer, recv_bytes, &metadata_client);

			if (send_bytes < 0)
			{
				if (send_bytes == SRT_EASYNCSND)
				{
					if (app->full_buffer == FULL_BUFFER_DROP)
						fprintf(stderr, "Buffer for client is full - packed dropped!\n");
					else if (app->full_buffer == FULL_BUFFER_KILL)
					{
						fprintf(stderr, "Buffer for client is full - client killed!\n");
						srt_close(client->socket);
						DL_DELETE(app->clients, client);
					}
				}
				else
				{
					fprintf(stderr, "Client send error: %s\n", srt_getlasterror_str());
					srt_close(client->socket);
					DL_DELETE(app->clients, client);
				}
			}
		}
	}

	fprintf(stderr, "Publisher disconnected: %s\n", srt_getlasterror_str());
	client_free(app->publish_socket);
	app->publish_socket = NULL;

	if (!app->keep_alive)
		client_list_free(&app->clients);

	free(app->buffer);
	app->buffer = NULL;
	app->buffer_size = 0;
	app->thread = 0;

	pthread_detach(pthread_self()); // detach to release resources
}

void* publish_accept_thread(void* arg)
{
	Server* server = (Server*)arg;
	struct sockaddr_storage addr;

	while (true)
	{
		int addr_len = sizeof(addr);
		SRTSOCKET publish_socket = srt_accept(server->publish_server, (struct sockaddr*)&addr, &addr_len);
		error_assert_srt(publish_socket);

		Client* client = client_init(publish_socket, &addr);

		// load payload and streamid
		int32_t payload_size;
		int payload_size_len = sizeof(payload_size);
		srt_getsockflag(publish_socket, SRTO_PAYLOADSIZE, &payload_size, &payload_size_len);

		int streamid_len = MAX_STREAMID_LENGTH;
		char streamid[streamid_len];
		srt_getsockflag(publish_socket, SRTO_STREAMID, streamid, &streamid_len);

		if (streamid_len == 0)
			strcpy(streamid, server->config->default_app_name);

		printf("publisher accepted - %s with port %d (%s)\n", client->ip, client->port, streamid);

		// get app struct and set data
		App* app = NULL;
		HASH_FIND_STR(server->apps, streamid, app);

		app->publish_socket = client;
		app->buffer = (char*)malloc_safe(payload_size);
		app->buffer_size = payload_size;

		// start sending thread
		error_assert(pthread_create(&app->thread, NULL, publish_send_thread, (void*)app) != 0, "pthread_create failed",
					 EXIT_PTHREAD_ERROR);
	}
}

void* client_accept_thread(void* arg)
{
	Server* server = (Server*)arg;
	struct sockaddr_storage addr;

	while (true)
	{
		int addr_len = sizeof(addr);
		SRTSOCKET client_socket = srt_accept(server->client_server, (struct sockaddr*)&addr, &addr_len);
		error_assert_srt(client_socket);

		Client* client = client_init(client_socket, &addr);

		// load streamid
		int streamid_len = MAX_STREAMID_LENGTH;
		char streamid[streamid_len];
		srt_getsockflag(client_socket, SRTO_STREAMID, streamid, &streamid_len);

		if (streamid_len == 0)
			strcpy(streamid, server->config->default_app_name);

		printf("client accepted - %s with port %d (%s)\n", client->ip, client->port, streamid);

		// get app struct
		App* app = NULL;
		HASH_FIND_STR(server->apps, streamid, app);

		// set blocking or non-blocking write based on FullBufferBehaviour
		bool sync = app->full_buffer == FULL_BUFFER_WAIT;
		srt_setsockflag(client_socket, SRTO_SNDSYN, &sync, sizeof(sync));

		pthread_mutex_lock(&app->waiting_clients_mutex);
		DL_APPEND(app->waiting_clients, client);
		pthread_mutex_unlock(&app->waiting_clients_mutex);
	}
}

Server* server_init(Conf* conf)
{
	Server* server = (Server*)malloc_safe(sizeof(Server));
	server->config = conf;
	server->apps = NULL;

	error_assert_srt(srt_startup());
	srt_setloglevel(conf->log_level);

	server->publish_server = create_socket(conf->publish_port, conf->ip, validate_peer_publish, server);
	server->client_server = create_socket(conf->client_port, conf->ip, validate_peer_client, server);

	start_accept_thread(&server->publish_listen_thread, publish_accept_thread, server);
	start_accept_thread(&server->client_listen_thread, client_accept_thread, server);

	return server;
}

void server_stop(Server* server)
{
	pthread_cancel(server->client_listen_thread);
	pthread_cancel(server->publish_listen_thread);

	pthread_join(server->client_listen_thread, NULL);
	pthread_join(server->publish_listen_thread, NULL);

	App* tmp;
	App* item;
	HASH_ITER(hh, server->apps, item, tmp)
	{
		HASH_DEL(server->apps, item);
		app_free(item);
	}

	srt_close(server->publish_server);
	srt_close(server->client_server);

	free(server);
}
