#include "conf.h"
#include "types.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Conf* conf_default()
{
	Conf* conf = (Conf*)malloc_safe(sizeof(Conf));

	conf->log_level = LOG_ERR;

	conf->publish_port = 20099;
	conf->client_port = 20100;

	conf->ip = IP_VERSION_BOTH;

	// empty passphrases
	conf->publish_passphrase[0] = '\0';
	conf->client_passphrase[0] = '\0';

	strcpy(conf->default_app_name, "default");

	conf->accept_unknown_app = true;
	conf->keep_alive = false;

	conf->full_buffer = FULL_BUFFER_KILL;
}

void conf_fill_app_default(Conf* conf, App* app)
{
	if (strlen(app->name) == 0) // no streamid
		strcpy(app->name, conf->default_app_name);

	strcpy(app->publish_passphrase, conf->publish_passphrase);
	strcpy(app->client_passphrase, conf->client_passphrase);

	app->keep_alive = conf->keep_alive;
	app->full_buffer = conf->full_buffer;
}

void load_port(Conf* conf, const char* path, unsigned short* value)
{
	int tmp;
	if (config_lookup_int(&conf->config, path, &tmp))
	{
		if (tmp <= 0 || tmp > 65535)
			fprintf(stderr, "Warning: Invalid %s in config file.\n", path);
		else
			*value = (unsigned short)tmp;
	}
}

void load_string(Conf* conf, const char* path, char* value, int max_length)
{
	const char* tmp;
	if (config_lookup_string(&conf->config, path, &tmp))
	{
		if (strlen(tmp) > max_length)
			fprintf(stderr, "Warning: Invalid %s in config file.\n", path);
		else
			strcpy(value, tmp);
	}
}

void load_setting_string(config_setting_t* setting, const char* path, char* value, int max_length)
{
	const char* tmp;
	if (config_setting_lookup_string(setting, path, &tmp))
	{
		if (strlen(tmp) > max_length)
			fprintf(stderr, "Warning: Invalid %s.%s in config file.\n", setting->name, path);
		else
			strcpy(value, tmp);
	}
}

void load_bool(Conf* conf, const char* path, bool* value)
{
	int tmp;
	if (config_lookup_int(&conf->config, path, &tmp))
		*value = (bool)tmp;
}

void load_setting_bool(config_setting_t* setting, const char* path, bool* value)
{
	int tmp;
	if (config_setting_lookup_bool(setting, path, &tmp))
		*value = (bool)tmp;
}

void load_enum_full_buffer(Conf* conf, const char* path, FullBufferBehaviour* value)
{
	int tmp;
	if (config_lookup_int(&conf->config, path, &tmp))
	{
		if (tmp <= 0 || tmp > ENUM_FULL_BUFFER_COUNT)
			fprintf(stderr, "Warning: Invalid %s in config file.\n", path);
		else
			*value = (FullBufferBehaviour)tmp;
	}
}

// TODO: Interpret enum as string
void load_setting_enum_full_buffer(config_setting_t* setting, const char* path, FullBufferBehaviour* value)
{
	int tmp;
	if (config_setting_lookup_int(setting, path, &tmp))
	{
		if (tmp <= 0 || tmp > ENUM_FULL_BUFFER_COUNT)
			fprintf(stderr, "Warning: Invalid %s in config file.\n", path);
		else
			*value = (FullBufferBehaviour)tmp;
	}
}

void load_enum_ip_version(Conf* conf, const char* path, IpVersion* value)
{
	int tmp;
	if (config_lookup_int(&conf->config, path, &tmp))
	{
		if (tmp <= 0 || tmp > ENUM_IP_VERSION_COUNT)
			fprintf(stderr, "Warning: Invalid %s in config file.\n", path);
		else
			*value = (IpVersion)tmp;
	}
}

void load_log_level(Conf* conf, const char* path, int* value)
{
	int tmp;
	if (config_lookup_int(&conf->config, path, &tmp))
	{
		if (tmp < LOG_EMERG || tmp > LOG_DEBUG)
			fprintf(stderr, "Warning: Invalid %s in config file.\n", path);
		else
			*value = tmp;
	}
}

Conf* conf_load(const char* path)
{
	Conf* conf = conf_default();
	config_init(&conf->config);

	if (!config_read_file(&conf->config, path)) // TODO: File not exists -> create
	{
		print_error(config_error_text(&conf->config), "libconfig", config_error_file(&conf->config),
					config_error_line(&conf->config));
		exit(EXIT_FAILURE);
	}

	load_log_level(conf, "log_level", &conf->log_level);

	load_port(conf, "publish_port", &conf->publish_port);
	load_port(conf, "client_port", &conf->client_port);

	load_enum_ip_version(conf, "ip", &conf->ip);

	load_string(conf, "publish_passphrase", conf->publish_passphrase, MAX_PASSPHRASE_LENGTH);
	load_string(conf, "client_passphrase", conf->client_passphrase, MAX_PASSPHRASE_LENGTH);

	load_string(conf, "default_app_name", conf->default_app_name, MAX_STREAMID_LENGTH);

	load_bool(conf, "accept_unknown_app", &conf->accept_unknown_app);
	load_bool(conf, "keep_alive", &conf->keep_alive);

	load_enum_full_buffer(conf, "full_buffer", &conf->full_buffer);
	return conf;
}

void conf_unload(Conf** conf)
{
	if (*conf == NULL)
		return;

	config_destroy(&(*conf)->config);
	free(*conf);
	*conf = NULL;
}

// app must have initialized name
bool conf_fill_app(Conf* conf, App* app)
{
	conf_fill_app_default(conf, app);

	config_setting_t* app_config = config_lookup(&conf->config, app->name);
	bool in_config = app_config != NULL;

	if (in_config)
	{
		load_setting_string(app_config, "publish_passphrase", app->publish_passphrase, MAX_PASSPHRASE_LENGTH);
		load_setting_string(app_config, "client_passphrase", app->client_passphrase, MAX_PASSPHRASE_LENGTH);

		load_setting_bool(app_config, "keep_alive", &app->keep_alive);
		load_setting_enum_full_buffer(app_config, "full_buffer", &app->full_buffer);
	}

	return in_config;
}